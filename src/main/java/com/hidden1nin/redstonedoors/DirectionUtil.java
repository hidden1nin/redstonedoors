package com.hidden1nin.redstonedoors;

import org.bukkit.block.BlockFace;

public class DirectionUtil {
    public BlockFace getOppositeFace(BlockFace blockFace){
        if(blockFace ==BlockFace.DOWN) return BlockFace.UP;
        if(blockFace ==BlockFace.UP) return BlockFace.DOWN;
        if(blockFace ==BlockFace.NORTH) return BlockFace.SOUTH;
        if(blockFace ==BlockFace.SOUTH) return BlockFace.NORTH;
        if(blockFace ==BlockFace.WEST) return BlockFace.EAST;
        if(blockFace ==BlockFace.EAST) return BlockFace.WEST;
        return null;
    }
}
