package com.hidden1nin.redstonedoors;

import org.bukkit.plugin.java.JavaPlugin;


public final class RedstoneDoors extends JavaPlugin {
    private static RedstoneDoors plugin;
    private static DirectionUtil directionUtil;
    private static Config configHandler;

    public static RedstoneDoors getPlugin() {
        return plugin;
    }

    public static DirectionUtil getDirectionUtil() {
        return directionUtil;
    }

    public static Config getConfigHandler() {
        return configHandler;
    }


    @Override
    public void onEnable() {
        // Plugin startup logic
        plugin = this;
        directionUtil = new DirectionUtil();
        configHandler = new Config();
        configHandler.setupConfig();
        getServer().getPluginManager().registerEvents(new PistonEvent(), this);

        //this.getCommand("placeholder").setExecutor(new CommandPlace());
    }


    @Override
    public void onDisable() {
        // Plugin shutdown logic
    }
}
