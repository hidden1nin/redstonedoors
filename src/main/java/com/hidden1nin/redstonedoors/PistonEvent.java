package com.hidden1nin.redstonedoors;

import org.bukkit.Material;
import org.bukkit.block.BlockFace;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockPistonExtendEvent;
import org.bukkit.event.block.BlockPistonRetractEvent;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.ArrayList;

public class PistonEvent implements Listener {
    private final ArrayList<Material> blocked = RedstoneDoors.getConfigHandler().getUnMovable();

    @EventHandler
    public void onPistonExtend(BlockPistonExtendEvent blockPistonEvent) {
        if (blockPistonEvent.getDirection() != BlockFace.DOWN) {
            return;
        }
        if (blocked.contains(blockPistonEvent.getBlock().getLocation().clone().add(0, -2, 0).getBlock().getType()))
            return;
        if (blocked.contains(blockPistonEvent.getBlock().getLocation().clone().add(0, -3, 0).getBlock().getType()))
            return;
        if (blocked.contains(blockPistonEvent.getBlock().getLocation().clone().add(0, -4, 0).getBlock().getType()))
            return;
        if (blockPistonEvent.getBlock().getRelative(BlockFace.UP).getType() == Material.IRON_BLOCK || blockPistonEvent.getBlock().getRelative(BlockFace.UP).getType() == Material.EMERALD_BLOCK) {
            //iron block is directly above piston, then an air gap or allows floor
            if (blockPistonEvent.getBlock().getLocation().clone().add(0, 3, 0).getBlock().getType() == Material.AIR) {
                blockPistonEvent.getBlock().getLocation().clone().add(0, 3, 0).getBlock().setType(blockPistonEvent.getBlock().getLocation().clone().add(0, -2, 0).getBlock().getType());
                blockPistonEvent.getBlock().getLocation().clone().add(0, -2, 0).getBlock().setType(Material.AIR);
            }
            new BukkitRunnable() {
                public void run() {
                    if (blockPistonEvent.getBlock().getLocation().clone().add(0, 4, 0).getBlock().getType() == Material.AIR) {

                        blockPistonEvent.getBlock().getLocation().clone().add(0, 4, 0).getBlock().setType(blockPistonEvent.getBlock().getLocation().clone().add(0, -3, 0).getBlock().getType());
                        blockPistonEvent.getBlock().getLocation().clone().add(0, -3, 0).getBlock().setType(Material.AIR);
                    }
                }
            }.runTaskLater(RedstoneDoors.getPlugin(), 5L);
            new BukkitRunnable() {

                public void run() {
                    if (blockPistonEvent.getBlock().getRelative(BlockFace.UP).getType() == Material.EMERALD_BLOCK) {
                        if (blockPistonEvent.getBlock().getLocation().clone().add(0, 5, 0).getBlock().getType() == Material.AIR) {
                            blockPistonEvent.getBlock().getLocation().clone().add(0, 5, 0).getBlock().setType(blockPistonEvent.getBlock().getLocation().clone().add(0, -4, 0).getBlock().getType());
                            blockPistonEvent.getBlock().getLocation().clone().add(0, -4, 0).getBlock().setType(Material.AIR);
                        }
                    }
                }
            }.runTaskLater(RedstoneDoors.getPlugin(), 10L);

        }

    }

    @EventHandler
    public void onPistonRetract(BlockPistonRetractEvent blockPistonEvent) {
        if (blockPistonEvent.getDirection() != BlockFace.DOWN) {
            return;
        }
        if (blocked.contains(blockPistonEvent.getBlock().getLocation().clone().add(0, 3, 0).getBlock().getType()))
            return;
        if (blocked.contains(blockPistonEvent.getBlock().getLocation().clone().add(0, 4, 0).getBlock().getType()))
            return;
        if (blocked.contains(blockPistonEvent.getBlock().getLocation().clone().add(0, 5, 0).getBlock().getType()))
            return;
        if (blockPistonEvent.getBlock().getRelative(BlockFace.UP).getType() == Material.IRON_BLOCK || blockPistonEvent.getBlock().getRelative(BlockFace.UP).getType() == Material.EMERALD_BLOCK) {
            new BukkitRunnable() {
                public void run() {
                    if (blockPistonEvent.getBlock().getLocation().clone().add(0, -2, 0).getBlock().getType() == Material.AIR) {
                        blockPistonEvent.getBlock().getLocation().clone().add(0, -2, 0).getBlock().setType(blockPistonEvent.getBlock().getLocation().clone().add(0, 3, 0).getBlock().getType());
                        blockPistonEvent.getBlock().getLocation().clone().add(0, 3, 0).getBlock().setType(Material.AIR);
                    }
                }
            }.runTaskLater(RedstoneDoors.getPlugin(), 10L);
            new BukkitRunnable() {

                public void run() {
                    if (blockPistonEvent.getBlock().getLocation().clone().add(0, -3, 0).getBlock().getType() == Material.AIR) {
                        blockPistonEvent.getBlock().getLocation().clone().add(0, -3, 0).getBlock().setType(blockPistonEvent.getBlock().getLocation().clone().add(0, 4, 0).getBlock().getType());
                        blockPistonEvent.getBlock().getLocation().clone().add(0, 4, 0).getBlock().setType(Material.AIR);
                    }
                }
            }.runTaskLater(RedstoneDoors.getPlugin(), 5L);

            if (blockPistonEvent.getBlock().getRelative(BlockFace.UP).getType() == Material.EMERALD_BLOCK) {
                if (blockPistonEvent.getBlock().getLocation().clone().add(0, -4, 0).getBlock().getType() == Material.AIR) {
                    blockPistonEvent.getBlock().getLocation().clone().add(0, -4, 0).getBlock().setType(blockPistonEvent.getBlock().getLocation().clone().add(0, 5, 0).getBlock().getType());
                    blockPistonEvent.getBlock().getLocation().clone().add(0, 5, 0).getBlock().setType(Material.AIR);
                }
            }
        }


    }

}

