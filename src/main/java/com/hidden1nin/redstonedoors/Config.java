package com.hidden1nin.redstonedoors;

import org.bukkit.Material;

import java.util.ArrayList;

public class Config {
    public ArrayList<Material> getUnMovable(){
        ArrayList<Material> bad = new ArrayList<>();
        if(!RedstoneDoors.getPlugin().getConfig().contains("Black-List")) return bad;
        for(String block :RedstoneDoors.getPlugin().getConfig().getStringList("Black-List")){
            if(Material.getMaterial(block.toUpperCase()) != null) {
                bad.add(Material.getMaterial(block.toUpperCase()));
            }

        }

        return bad;
    }
    public void setupConfig(){
        if(!RedstoneDoors.getPlugin().getConfig().contains("Black-List")){
            ArrayList<String> blocked = new ArrayList<>();
            blocked.add("OBSIDIAN");
            blocked.add("BEDROCK");
            RedstoneDoors.getPlugin().getConfig().set("Black-List",blocked);
            RedstoneDoors.getPlugin().saveConfig();
        }
    }
}
